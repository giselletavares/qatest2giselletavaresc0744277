//
//  Infection.swift
//  ScreamItus-IOS
//
//  Created by parrot on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import Foundation
class Infection {
    
    var infectByFirstDays = 5
    var infectAfter7Days = 8
    
    func calculateTotalInfected(day: Int) -> Int {
        
        if day <= 0 {
            return -1
        } else if day % 2 == 0 {
            return 0
        } else if day < 8 {
            return day * infectByFirstDays
        }
        return (7 * infectByFirstDays) + ((day - 7) * infectAfter7Days)
        
    }
    
}
