//
//  ScreamItus_IOSTests.swift
//  ScreamItus-IOSTests
//
//  Created by Giselle Tavares on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

@testable import ScreamItus_IOS

class ScreamItus_IOSTests: XCTestCase {
    
    let infection:Infection = Infection()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // RC1
    func testNumberOfDaysGreaterThanOne(){
        XCTAssertEqual(5, infection.calculateTotalInfected(day: 1))
    }
    
    func testNumberOfDaysLessThanOne(){
        XCTAssertEqual(-1, infection.calculateTotalInfected(day: 0))
    }
    
    // RC2
    func testNumberOfInfectsInstructors(){
        XCTAssertEqual(5, infection.calculateTotalInfected(day: 1))
    }
    
    // RC3
    func testNumberOfInfectsInstructorsDaysGreaterThan7(){
        XCTAssertEqual(51, infection.calculateTotalInfected(day: 9))
    }
    
    // RC4
    func testNumberOfInfectsWithImmuneInstructors(){
        XCTAssertEqual(0, infection.calculateTotalInfected(day: 8))
    }

}
