//
//  ScreamItus_IOSUITests.swift
//  ScreamItus-IOSUITests
//
//  Created by Giselle Tavares on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

class ScreamItus_IOSUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // TC1
    func testResultLabelHidden(){
        let app = XCUIApplication()
        
        let textBox = app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField).element
        XCTAssertEqual(true, textBox.exists)
        
        let buttonCalculate = app.buttons["Calculate"]
        XCTAssertEqual(true, buttonCalculate.exists)
        
        let labelText = app.staticTexts["1000 instructors infected!"]
        XCTAssertEqual(false, labelText.exists)
        
    }
    
    func testCorrectNumberInfected(){
        let app = XCUIApplication()
        
        let textBox = app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField).element
        textBox.tap()
        textBox.setText("3", app) as? UITextField
        
        
        
        
        
        
        
        
        
        
    }

//    func testExample() {
//        // Use recording to get started writing UI tests.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//
//        let app = XCUIApplication()
//        app.otherElements.containing(.staticText, identifier:"Infection Calculator").children(matching: .textField).element.tap()
//        app.buttons["Calculate"].tap()
//        app.staticTexts["15 instructors infected"].tap()
//
//
//    }

}
